#!/usr/bin/env python3

class sums:
    def __init__(self):
        self.__result=0.0
    def add(self, a):
        self.__result += a
        return self.__result
    def subtract(self, a):
        self.__result -= a
    def get_result(self):
        return self.__result

if __name__ == "__main__":
    x = sums()
    print(x.add(5))
    x.subtract(2)
    print(x.get_result())
