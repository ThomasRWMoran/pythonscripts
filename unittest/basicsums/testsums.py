#!/usr/bin/env python3
import unittest
import sums

class test_sums(unittest.TestCase):

    def test_add(self):
        x = sums.sums()
        print('Running add')
        assert x.add(3) == 3, 'The add method failed to add correctly'

    def test_subtract(self):
        x = sums.sums()
        print('\nRunning subtract')
        x.add(5)
        x.subtract(3)
        assert x.get_result() == 2, 'The subtract method fails to subtract correctly'

if __name__ == "__main__":
    unittest.main()
