#!/usr/bin/env python3

def collatz(num):
    if num % 2 == 0:
        number = int(num / 2)
    else:
        number = (3*num)+1
    return number

check = 3
limiter = 100
while True:
    runner = check
    count = 0
    while runner > 1 and count < limiter:
        runner = collatz(runner)
        count+=1
    if count == limiter:
        print(check, "hit a", limiter,"steps")
        break
    else:
        print(check)
        check+=1
