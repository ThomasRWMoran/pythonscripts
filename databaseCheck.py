#!/usr/bin/env python3
import mysql.connector
try:
  dbconn = mysql.connector.connect(user='thomas',
                                   host='mysql.grads.al-labs.co.uk',
                                   database='stevecontact',
                                   password='secret')
except:
  print("Cannot connect to DB server")
  exit(1)
dbcur=dbconn.cursor(dictionary=True)
dbcur.execute("SELECT * FROM contacts;")
# Getting data from the database
for row in dbcur:
  print("Name of Contact: "+row['firstname']+" "+row['lastname'])
# Insert some data into contacts
dbcur.execute("INSERT INTO contacts(firstname,lastname,phone) VALUES('a','b','1');")
if dbcur.lastrowid:
  print("Insert id: "+str(dbcur.lastrowid))
else:
  print("Last insert failed")
dbconn.commit()
dbcur.close()
dbconn.close()
