#!/usr/bin/env python3
arr = [1,2,3,4,5,6,7]

print(list(map(lambda x: x*x,arr)))

names = [ 'Luke Skywalker', 'Han Solo', 'Leia Organa' ]

def get_surname(name):
    return name.split()[1]

print(list(map(get_surname,names)))
names.sort(key=get_surname)
print(names)
names.sort()
print(names)
