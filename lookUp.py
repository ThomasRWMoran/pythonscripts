#!/usr/bin/env python3

def lookup(s, char):
    if char in s:
        return s.index(char)

s = 'asfongbfsoboavad'

val = lookup(s, 'g')
if val:
    print(val)

val = lookup(s, 'X')
if val is None:
    print("Found a None value")
else:
    print(val)
