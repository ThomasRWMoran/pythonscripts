#!/usr/bin/env python3
import sys

def string_is_int(string):
    try: # this triggers error handling
        if float(string) % 1 == 0: # if the remainder of string / 1 is 0
        # then it is an integer
        # but if the string can't be converted to a float
        # it will 'throw' an error (and skip code)
            return True
        else:
            return False
    except ValueError: # this 'catches' the error
        return False

def int_is_even(integer):
    if integer % 2 == 0:
        return True
    else:
        return False

def request_integer():
    while True:
        Input = input("Please give me a positive integer: ")
        if string_is_int(Input) and int(Input) > 0:
            break
        elif string_is_int(Input) and int(Input) <= 0:
            print("I said a positive integer.")
        else:
            print("That's not an integer.")
    return int(Input)

def factor_check(int1, int2):
    if int1 % int2 == 0:
        return True
    else:
        return False

number = request_integer()

if int_is_even(number):
    print("Number is not prime, it is even")
    sys.exit(0)
else:
    test = 3
    while test <= (number/2):
        if factor_check(number, test):
            print("Number is not prime, as", test, "is a factor")
            sys.exit(0)
        else:
            test += 1
    print("Number is prime")
