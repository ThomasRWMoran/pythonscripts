#!/usr/bin/env python3
import json
import requests
import sys

r = requests.get('https://restcountries.eu/rest/v2/all')

if r.status_code != 200:
    print("Request error")
    sys.exit(1)

rjson = json.loads(r.content)

count = 0
for country in rjson:
    if len(country.get('languages')) > 1:
        print(country.get('name'))
        count+=1
        for language in country.get('languages'):
            print("\t=>",language.get('name'))

print(count, "countries have more than one language")
