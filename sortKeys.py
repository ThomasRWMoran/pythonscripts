#!/usr/bin/env python3

def my_sort(element):
    return len(element)

l1 = [ 'cat', 'a', 'beat' ]

l1.sort(key=my_sort)
print(l1)
