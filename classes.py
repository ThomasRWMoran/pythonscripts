#!/usr/bin/env python3
class LivingThing:
    def get_age(self):
        return 10

class Animal(LivingThing):
    def __init__(self, name):
        self.name = name
        self.haveSpoken = False
    def speak(self):
        self.haveSpoken = True

class Human(Animal):
    def speak(self):
        print("Hello, my name is", self.name)

class Dog(Animal):
    def speak(self):
        print("WOOF WOOF WOOF WOOF!!!")

class Cow(Animal):
    def speak(self):
        if not self.haveSpoken:
            print("Mooooooo")

class Cat(Animal):
    def speak(self):
        if self.haveSpoken:
            print("I won't repeat myself")
        else:
            print("What up, my name's",self.name,"and I'm a cat")
            self.haveSpoken = True

class Plant(LivingThing):
    def get_age(self):
        return 5


tree = Plant()
me = Human('Tom')
baxter = Dog("Baxter")
a_cow = Cow("Who cares what the cow is called")
rey = Cat("Rey")

me.speak()
baxter.speak()
a_cow.speak()
rey.speak()
me.speak()
baxter.speak()
a_cow.speak()
rey.speak()
