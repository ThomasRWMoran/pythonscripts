#!/usr/bin/env python3

def collatz(num):
    print(num)
    if num == 1:
        count = 1
    elif num % 2 == 0:
        count = collatz(int(num/2))
        count+=1
    else:
        count = collatz(3*num+1)
        count+=1
    return count

number = int(input("Insert a number: "))
count = collatz(number)
print("There were", count, "steps")
