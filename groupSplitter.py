#!/usr/bin/env python3
def get_username():
    person_name = input("Please enter your name: ")
    return person_name

def get_age():
    return int(input("Please enter your age: "))

name = get_username()
age = get_age()

if name[0].lower() <= 'm' and age <= 25:
    print("Group 1")
elif name[0].lower() <= 'm' and age > 25:
    print("Group 2")
elif name[0].lower() > 'm' and age <= 25:
    print("Group 3")
else:
    print("Group 4")
