#!/usr/bin/env python3
import json
import sys
import requests


def getPayload(message):
    dPayload = {"channel": "#academy_api_testing", "username": "tommy boy", "text": "Default from Toms python"}
    if type(message) != str:
        print('Message not string')
        sys.exit(3)
    else:
        dPayload['text'] = message
    jPayload = json.loads(json.dumps(dPayload))
    return jPayload

def getCountries():
    COUNTRIES_URL = 'https://restcountries.eu/rest/v2/all'
    r = requests.get(COUNTRIES_URL)
    if r.status_code != 200:
        print("requests.get error")
        sys.exit(1)
    rjson = json.loads(r.content)
    return rjson

def send2Slack(url, message):
    rSlack = requests.post(url, json=getPayload(message))
    if rSlack.status_code != 200:
        print("requests.post error")
        sys.exit(2)

def sortCountriesByPop(countries):
    sortedCountries = sorted(countries, key=lambda k: int(k['population']), reverse=True)
    return sortedCountries

def getCountriesMessage(countries):
    sortedCountries = sortCountriesByPop(countries)
    sMessage = 'The top ten most populous countries are:\n'
    for i in range(0, 10):
        sMessage += (sortedCountries[i]['name'] + ': ' + str(sortedCountries[i]['population']) + '\n')
    return sMessage

SLACK_URL = ""

#cMessage = getCountriesMessage(getCountries())

send2Slack(SLACK_URL, sys.argv[1])
