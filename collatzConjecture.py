#!/usr/bin/env python3

def collatz(num):
    if num % 2 == 0:
        number = int(num / 2)
    else:
        number = (3*num)+1
    return number

name = "yes"
while name == "yes":
    while True:
        try:
            number = int(input("Test an number: "))
            break
        except ValuteError:
            print("That's not a number")

    runner = number
    count = 0
    print(number,"=>")

    while runner != 1:
        runner = collatz(runner)
        if runner == 1:
            print(runner)
        else:
            print(runner,"=>")
        count+=1

    print("\nWe started on", number,"and it took",count,"to get to 1")
    name = input("Run another one? (yes/no) ")

print("Okay bye")
