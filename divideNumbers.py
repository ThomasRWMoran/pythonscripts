#!/usr/bin/env python3
import sys

def requestNumber():
    while True:
        try:
            number = float(input("Test an number: "))
            if number % 1 == 0:
                number = int(number)
            break
        except ValueError:
            print("That's not a number")
    return number

def divide(num1, num2):
    try:
        answer = num1/num2
        message = str(num1) + ' divided by ' + str(num2) + ' is ' + str(answer)
        print(message)
    except TypeError:
        print("It's not your fault, Tom probably forgot to convert to string again")
        sys.exit(1)
    except ZeroDivisionError:
        print("Your second number was zero, you absolute buffoon")

print("FIRST NUMBER!")
nOne = requestNumber()
print("SECOND NUMBER!")
nTwo = requestNumber()
divide(nOne,nTwo)
