#!/usr/bin/env python3

d = {'a': 1, 'b': 2, 'c': 3}

try:
    print(d['a'])
    magic = 0/0
except KeyError:
    print("Couldn't find d['d']")
except ArithmeticError:
    print("Can't do that silly")

print("All done")
