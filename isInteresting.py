#!/usr/bin/env python3

l1 = [1,2,3,4,5,6,7,8,9]
evenitems = []

def is_interesting(item):
    if item % 2 == 0:
        return True
    else:
        return False

#for item in l1:
#    if is_interesting(item):
#        print(item, "is even")
#        evenitems.append(item)

evenitems = list(filter(is_interesting,l1))
print("Interesting items: ", evenitems)
